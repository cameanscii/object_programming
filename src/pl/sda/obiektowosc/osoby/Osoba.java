package pl.sda.obiektowosc.osoby;

public class Osoba {
    String imie;
    int wiek;

    public void przedstawSie() {
        System.out.printf("\nCześć! Mam na imię %s i mam %d lat.", imie, wiek);
    }

    public static void main(String[] args) {

        Osoba mateusz =new Osoba();
        mateusz.imie="Mateusz";
        mateusz.wiek=28;
        mateusz.przedstawSie();

        Osoba ania = new Osoba();
        ania.imie = "Ania";
        ania.wiek = 25;
        ania.przedstawSie();

        Osoba andrzej = new Osoba();
        andrzej.imie = "Andrzej";
        andrzej.wiek = 54;
        andrzej.przedstawSie();

        Osoba mariola = new Osoba();
        mariola.imie = "Mariola";
        mariola.wiek = 68;
        mariola.przedstawSie();
    }

}