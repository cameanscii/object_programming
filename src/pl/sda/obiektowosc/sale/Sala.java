package pl.sda.obiektowosc.sale;

public class Sala {
    String nazwa;
    double iloscM2;
    int liczbaStanowisk;
    boolean czyJestRzutnik;
    boolean czyJestWolna = true;

    public Sala(String nazwa, double iloscM2, int liczbaStanowisk, boolean czyJestRzutnik) {
        this.nazwa = nazwa;
        this.iloscM2 = iloscM2;
        this.liczbaStanowisk = liczbaStanowisk;
        this.czyJestRzutnik = czyJestRzutnik;
    }

    public Sala(){}

    void wyswietlOpisSali(){
        String opis = String.format("Sala %s o pow. %.2f, " +
                "liczba stanowisk: %d", nazwa, iloscM2, liczbaStanowisk);
        if (czyJestRzutnik){
            opis += ", sala posiada rzutnik";
        }else {
            opis += ", sala nie posiada rzutnika";
        }
        System.out.println(opis);
    }



}
