package pl.sda.obiektowosc.sale;

public class Main {
    public static void main(String[] args) {
        Sala gdansk = new Sala();
        gdansk.nazwa ="Gdańsk";
        gdansk.iloscM2=30.05;
        gdansk.liczbaStanowisk=8;
        gdansk.czyJestRzutnik=true;

        Sala sztukaWyboru = new Sala();
        sztukaWyboru.nazwa="Java";
        sztukaWyboru.iloscM2=45;
        sztukaWyboru.liczbaStanowisk=12;
        sztukaWyboru.czyJestRzutnik = false;

        Sala nowa = new Sala ("Nowa", 100.02, 30, false);

        Sala[] zarzadzaneSale = new Sala[] {gdansk,sztukaWyboru,nowa};


        Menadzer jan = new Menadzer();
        jan.imie="Jan";
        jan.sale=zarzadzaneSale;
        jan.wyswietlDostepneSale();
        jan.zabookujSale("Java");
        jan.wyswietlDostepneSale();

    }
}
